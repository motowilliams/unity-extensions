﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Practices.Unity;

namespace UnityExtensions
{
	public static class UnityExtensions
	{
		public static InjectionConstructor AsCtorArg(this string value)
		{
			return new InjectionConstructor(value);
		}

		public static LifetimeManager AsSingleton()
		{
			return new ContainerControlledLifetimeManager();
		}

		public static IUnityContainer RegisterSingleton<T, TInstance>(this IUnityContainer container)
			where T : class
			where TInstance : T
		{
			container.RegisterType<T, TInstance>(new ContainerControlledLifetimeManager());
			return container;
		}

		public static IUnityContainer RegisterSingletons<T>(this IUnityContainer container) where T : class
		{
			IEnumerable<Type> types = Assembly
				.GetExecutingAssembly()
				.GetTypes()
				.Where(x => typeof(T).IsAssignableFrom(x) && x.IsInterface == false);
			foreach (var type in types)
			{
				container.RegisterType(typeof(T), type, type.Name, new ContainerControlledLifetimeManager());
			}
			return container;
		}

		public static IUnityContainer RegisterAllOf<T>(this IUnityContainer container, bool singleton = false) where T : class
		{
			IEnumerable<Type> types = Assembly
				.GetExecutingAssembly()
				.GetTypes()
				.Where(x => typeof(T).IsAssignableFrom(x) && x.IsInterface == false);
			foreach (var type in types)
			{
				if (singleton)
					container.RegisterType(typeof(T), type, type.Name, new ContainerControlledLifetimeManager());
				else
					container.RegisterType(typeof(T));
			}
			return container;
		}

		public static IUnityContainer RegisterWithCtorArgs<T>(this IUnityContainer container, params object[] parameterValues)
		{
			container.RegisterWithCtorArgs<T>(false, parameterValues);
			return container;
		}

		public static void RegisterWithCtorArgs<T>(this IUnityContainer container, bool asSingleton,
												   params object[] parameterValues)
		{
			LifetimeManager lifetimeManager;
			if (asSingleton)
				lifetimeManager = new ContainerControlledLifetimeManager();
			else
				lifetimeManager = new TransientLifetimeManager();
			container
				.RegisterType<T>(
					lifetimeManager,
					new InjectionConstructor(parameterValues)
				);
		}

		public static IUnityContainer RegisterWithCtorArgs<T, TInstance>(this IUnityContainer container,
																		 params object[] parameterValues)
			where T : class
			where TInstance : T
		{
			container.RegisterWithCtorArgs<T, TInstance>(false, parameterValues);
			return container;
		}

		public static IUnityContainer RegisterWithCtorArgs<T, TInstance>(this IUnityContainer container, bool asSingleton,
																		 params object[] parameterValues)
			where T : class
			where TInstance : T
		{
			LifetimeManager lifetimeManager;
			if (asSingleton)
				lifetimeManager = new ContainerControlledLifetimeManager();
			else
				lifetimeManager = new TransientLifetimeManager();

			container
				.RegisterType<T, TInstance>(
					lifetimeManager,
					new InjectionConstructor(parameterValues)
				);
			return container;
		}



		public static IUnityContainer RegisterWithParameterArgs<T>(this IUnityContainer container,
																		 Dictionary<string, object> propertyInjectors)
			where T : class
		{
			container.RegisterWithParameterArgs<T>(false, propertyInjectors);
			return container;
		}

		public static IUnityContainer RegisterWithParameterArgs<T>(this IUnityContainer container, bool asSingleton,
																		 Dictionary<string, object> propertyInjectors)
			where T : class
		{
			LifetimeManager lifetimeManager;
			if (asSingleton)
				lifetimeManager = new ContainerControlledLifetimeManager();
			else
				lifetimeManager = new TransientLifetimeManager();

			container
				.RegisterType<T>(
					lifetimeManager,
					propertyInjectors.Select(x => new InjectionProperty(x.Key, x.Value)).ToArray()
				);
			return container;
		}

		public static IUnityContainer RegisterWithParameterArgs<T, TInstance>(this IUnityContainer container,
																		 Dictionary<string, object> propertyInjectors)
			where T : class
			where TInstance : T
		{
			container.RegisterWithParameterArgs<T, TInstance>(false, propertyInjectors);
			return container;
		}

		public static IUnityContainer RegisterWithParameterArgs<T, TInstance>(this IUnityContainer container, bool asSingleton,
																		 Dictionary<string, object> propertyInjectors)
			where T : class
			where TInstance : T
		{
			LifetimeManager lifetimeManager;
			if (asSingleton)
				lifetimeManager = new ContainerControlledLifetimeManager();
			else
				lifetimeManager = new TransientLifetimeManager();

			container
				.RegisterType<T, TInstance>(
					lifetimeManager,
					propertyInjectors.Select(x => new InjectionProperty(x.Key, x.Value)).ToArray()
				);
			return container;
		}

		public static string WhatDoIHave(this IUnityContainer container)
		{
			var stringBuilder = new StringBuilder();
			foreach (var registration in container.Registrations)
				stringBuilder.AppendLine(String.Format("{0} => {1}", registration.Name, registration.MappedToType));

			Console.WriteLine(stringBuilder.ToString());
			return stringBuilder.ToString();
		}
	}
}